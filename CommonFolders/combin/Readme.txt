Combinatorics - 

API_main_comb.py -  This script consists of two functions - an Encoder and a Decoder.

Encoder - 

function signature - encoder(wellID,Welldates,proddays,qoil,qwat,request_args,scale_args)

	wellID - The API_prod ID of the well

	Welldates - The report_dates of the well, stored as a list

	proddays - The proddays of the well, stored as a list

	qoil - Oil values of the well stored as a list

	qwat - Water values of the well stored as a list

	request_args - A list with two values that you want to encode. 
	Options are ['time','qoil','qwat','wor','cumoil','cumwat','gross','fo','fw']. Its case insensitive.

	scale_args - A list with two values that specify the scale of the values in request_args. Options are ['linear','log'].
	scale_args[0] will be applicable to request_args[0] while scale_args[1] will be applicable to request_args[1].

returns - A dictionary with the following keys {"WellID","arg1","arg2","opcode","Welldates","proddays"}
	
	wellID, Welldates,proddays same as above
	
	arg1, arg2 - Contains data specified by request_args with their scale conversions
	
	opcode - A four digit code that specifies the transformation applied on the data supplied to the encoder.
	

Decoder -  

function signature - decoder(encoded_data)

	encoded data is a dictionary which should have the same keys as returned by the encoder.
	{"WellID","arg1","arg2","opcode","Welldates","proddays"}
	
	returns - a dictionary with keys {"WellID","Welldates","proddays",x,y,"qoil","qwat"}
	
	where x and y are the original request_args supplied in the encoder. 
	And qoil and qwat will be present if they can be reverse engineered from the request_args.
	Possible with permutation of ['qoil','qwat','wor','cumoil','cumwat','gross'] 
	barring [qoil,cumoil] and [qwat,cumwat]
	

API_helpers_comb.py - This script contains helper functions for the main script. It also has functions to, establish connection to the database read well IDs and retrieve well data if required.

function signature - establish_DBconn(schema="public_data",user,password)
			returns cursor #database connection object

		   - get_WellPids(cursor,table_name)
			returns API_ids  #list of unique API_ids available in the table

		   - get_WellData(cursor,wellID,table_name)
			returns Welldates,proddays,qoil,qwat
