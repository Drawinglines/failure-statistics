# README #

This README would normally document whatever steps are necessary to get your application up and running.

### For Extracting Slopes before and after events (Reza) ###

* write the code for extraction of slopes in between events **(done)**
* write the code automatically to get well by well data from DB ** (done) **
* learn the hyper parameters of the trends ** (done) **
* use L1 trend for the points in between events ** (done) **
* write data to DB ** (done) **

### derivatives needed ###

* WOR **(done)**
* cumulative oil **(done)**
* cumulative water **(done)**
* oil rate **(done)**
* water rate **(done)**


### Detection and Prediction of Catastrophic Failures ###

* extraction of catastrophic failure times
* extraction of features before the CF
  * WOR 
  * qoil
  * slopes
  * FOP
  * oil price
  * etc
* building the predictive model

### extracting easy wells for the forecasting project
* extraction of CF times to be removed from analysis
* exporting the wells to DB