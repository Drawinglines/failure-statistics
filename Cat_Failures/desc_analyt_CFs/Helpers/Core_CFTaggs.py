# -*- coding: utf-8 -*-
"""
Created on Tue Mar 01 10:00:01 2016

@author: Administrator
"""
import reza_helpers as reza_hp
from collections import defaultdict

#====== global values
RefThrshYears = 1*12 # number of month if well was down at the end, then its CF




def CFTagger(data,NowDate):
        
        #----------------------------------------------------------------------
        # Analysis of CF Tags (The logic)        
        LastProdDay  = data['dates'][-1] # last date of production (logic in this as well)
        Abandon_DeltMonths = reza_hp.diff_month(LastProdDay, NowDate)
        
        if Abandon_DeltMonths > RefThrshYears: #if abandoned for more than 1 year (logic)
            CF_flag = 1 # well ended (its runlife is achieved)
            
        elif Abandon_DeltMonths<=2: #well still producing without fail
            CF_flag = 3
            #print "Fully Producing !!!"

        else: #well stopped but not sure CF or not, could be coming back soon
            CF_flag = 2
            #print "Stopped but not CF !!!"
        
        #----------------------------------------------------------------------
        
        
        #----------------------------------------------------------------------
        # gather needed data to be written to DB
        # Prepare dictionary data for writing
        WrDict = defaultdict(list)
        #======== Static dictionry to be written
        WrDict.update(data)
        WrDict['CF_flag'] = CF_flag
        #----------------------------------------------------------------------
        
        return WrDict