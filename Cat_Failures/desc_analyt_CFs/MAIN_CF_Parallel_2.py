'''
###############################################################################
#################### Written By @ Reza Khaninezhad March 2016 #################
###############################################################################
'''

# my modules
CommonDir = ''
import sys
sys.path.append(CommonDir+'Helpers') 

import reza_helpers as reza_hp
import DBWrite_helpers as DB_hp
import Core_CFTaggs as Core_CF
# python modules visualization
from collections import defaultdict
import pylab
from bprofile import BProfile
import time
from datetime import datetime

# python modules data frames
import cPickle as pk


from multiprocessing import Pool




#------------------------------
# parameterz and initialization
#NowDate = datetime.datetime.now()
DB_ProdTable_name   = "bigprod_ts11" #table to which the results have to be written
DB_MasterTable_name = "big_master"
ProdSchema          = "public_data"

    
#NowDate       = (str(now.year)+"-"+str(now.month)+"-"+str(now.day)) # "2015-04-30"
NowDate         = "2015-12-31" # "2015-04-30" "2015-05-31"
TrainStartDate  = "1900-01-01"
TrainEndDate    = NowDate
StateAPI        = 3   # for NorthDakuta = 33


Time_Trsh     = 2    # number of month the well should have been producing to be used for analysis
Diff_days     = 2    # tolerance on number of down days to be accepted as downevent
#-----------------------------------------
#-----------------------------------------
# Establish the connections
conn,cursor     = reza_hp.establish_DBconn()
#----------------------------------------- 
# Drop table if exists
Wrtable_name = "CFTags_Arkansas_SQL"
Wrschema     = "results"
try:
    query = "DROP TABLE {};".format(Wrschema+'.'+Wrtable_name)
    cursor.execute(query)
    conn.commit()
    print "Table Droped ... OK"
except:
    print "Table Doesnt Exist"
            
#-----------------------------------------
#load Unique Pids
unique_pids = reza_hp.get_WellPids(cursor, DB_ProdTable_name, DB_MasterTable_name, ProdSchema, StateAPI)
#unique_pids = reza_hp.get_TestingWellPids('test_ids_33')
#unique_pids  = [500106084]


NumofWells = len(unique_pids) #max number of wells to be analysed
def Well_Analyzer(args):
    i,inAPI,OilPrice_Dict = args
    #Wells to be analyzed
    print ("Analyzing Well Number: "+str(i)+" outof("+str(NumofWells-1)+")      With name: "+str(long(inAPI)))
    #========== Read Data From DB and preprocess them =========================
    
    dataorig    = reza_hp.get_WellData_unstr(cursor,DB_ProdTable_name,DB_MasterTable_name,ProdSchema,[inAPI],TrainStartDate,TrainEndDate,OilPrice_Dict)[0]
    data , flag = reza_hp.Preprocessing_DataDict(dataorig , Time_Trsh , Diff_days)
    if flag==0: # well has not produced (not good for analysis)
        print "Well not worthy of analysis !"
        return   
     
    #========== Analyze the data ==============================================
    AnsDict = Core_CF.CFTagger(data,NowDate)
    #==========================================================================
    
    
    #========== Write Well By Well ==============================================

    WrDict = defaultdict(list)
    WrDict['API']        = AnsDict['API_prod'][0]  
    WrDict['state']      = StateAPI   
    WrDict['CF_flag']    = AnsDict['CF_flag']
    WrDict['RunLife']    = AnsDict['RunLife']
    WrDict['EndDate']    = AnsDict['EndDate']
    WrDict['StartDate']  = AnsDict['StartDate']
    
    #WrDict         = {key:AnsDict[key] for key in ['a', 'b']} 
    _,_ = DB_hp.Write_Dict_ToDB(WrDict, Wrschema, Wrtable_name, conn, cursor, Format="SQL")
    
    
    #==========================================================================   
    return AnsDict
        
        
        
        
        
        
        
def main():
    start_time = time.time() 
    
    AnsDict_list = list()
    if __name__=='__main__': 
        print "Starting Analysis for " + str(len(unique_pids))+" Wells"
        #========== Read Data From DB =========================================
        OilPrice_Dict = reza_hp.get_oilprices(cursor , schema="public_data" , tablename="oil_price_monthly" , RefDates=None) # oil price monthly
        
        args = ((i,API,OilPrice_Dict) for i,API in enumerate(unique_pids))
        
        
        
        
        
        
        
        #===== using parallelprocessing
        
        pool  = Pool()
        tasks = pool.map_async(Well_Analyzer, args)
        tasks.wait()
        AnsDict_tuple = tasks.get()
        pool.close()
        pool.join()
        AnsDict_list= list(AnsDict_tuple)
        del AnsDict_tuple
        
        
        
        
        #===== using sequential processing
        '''
        for i,arg in enumerate(args): #NumofWells
                AnsDict_list.append(Well_Analyzer(arg)) 
        ''' 
                
              
              
              
              
              
              
              
        #===== remvoe none elements and change to tuple   
        AnsDict_list  = [x for x in AnsDict_list if x is not None]
        AnsDict_tuple = tuple(AnsDict_list)   
        del AnsDict_list        
        
        #======= save results into cpickle or jason file ======================
        #with open('results/NorthDakuta_AnalizedCFTags_tuple.p', 'wb') as fp:
        #    pk.dump(AnsDict_tuple, fp)
        print "Analyzed and Saved Data for " + str(len(AnsDict_tuple))+" Wells"
        #====================================================================== 
        
        #======= Close the DB connection
        cursor.close()
        conn.close()
    
        # time took the code
        EndTime = time.time()
        delt_t  = EndTime-start_time
        print ("Total Execution Time: ("+str(delt_t/60.0) + ") Minutes")
    
        
        #return WrDict_tuple
        return
        


#======= Run the Experiment ===================================================
    
# without profiler
#main()

# with profiler
profiler = BProfile('CF_Tagging.png')
with profiler:
    main()
