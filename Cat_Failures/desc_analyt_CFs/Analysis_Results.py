# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 14:21:39 2016

@author: Administrator
"""

# my modules
import reza_helpers as reza_hp
import DBWriters

# python modules
import pylab 
import numpy as np
from collections import defaultdict
import cPickle as pk

from combin import API_main_comb as combinator

from datetime import datetime

#from numba import jit
#from numpy import arange


#------------------------------
# parameterz and initialization
#NowDate = datetime.datetime.now()
NowDate = "2015-04-30"

Table_MT   = "tag_analysis"
Schema_MT  = "public_data"

Table_AUTO   = "Taha_NDK_prodTS"
Schema_AUTO  = "reza"

#------------------------------

def main():
    
    #----------------------
    # initialize
    pylab.close('all')
    #----------------------
    #-----------------------------------------
    # Establish the connections
    conn_auto,cursor_auto  = reza_hp.establish_DBconn(Schema_AUTO)
    conn_mt,cursor_mt      = reza_hp.establish_DBconn(Schema_MT)
    
    
    
    
    return
main()