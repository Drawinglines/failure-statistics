# -*- coding: utf-8 -*-
"""
Created on Tue Feb 16 10:17:45 2016

@author: Administrator
"""

import MySQLdb
import time
import numpy as np
import sys

import reza_helpers as reza_hp

##############################################################################################################################################################
def Write_Dict_ToDB(myDict,schema,table_name,conn=None,cursor=None,Format="NoSQL"):
    
    if conn==None:
        #-----------------------------------------
        # Establish the connections
        conn,cursor = reza_hp.establish_DBconn(schema)
        #-----------------------------------------
        # Drop table if exists
        try:
            query = "DROP TABLE {};".format(schema+'.'+table_name)
            cursor.execute(query)
            conn.commit()
        except:
            print "Table Doesnt Exist"
    
    
    
    
    
        
    dictkeys = myDict.keys()
    dictkeys.sort()
    sorted_dict_keys = dictkeys
    
    if Format=="SQL":
        sorted_dict_keys.insert(0,'EndDate')
        sorted_dict_keys.insert(0,'StartDate')
        sorted_dict_keys.insert(0,'API')   
    else:   
        sorted_dict_keys.insert(0,'dates')
        sorted_dict_keys.insert(0,'API')   
        
    sorted_dict_keys = SortedListSet(sorted_dict_keys)
    

    # Check whether the table already exists or not
    #ColTypes = "(API_master varchar(32) , StartDate  DATE , prod_date DATE , analysis INTEGER) "
    
    if Format=="SQL":
        # for writing like SQL type of table , dictioanry lists should have the smae number of elements
        varTypes = ['BIGINT', 'DATE' , 'DATE']+  ["DOUBLE" for i in xrange(len(sorted_dict_keys)-2) ]
        ColTypes = "(" + " , ".join([s+ " "+varTypes[i]   for i,s in enumerate(sorted_dict_keys)]) + ")"
        
    
    elif Format=="NoSQL":
        # for ts writing type of table 
        #ColTypes = "(API longtext , dates  longtext , proddays longtext , oil longtext , StartDate longtext) "
        ColTypes = "(" + " , ".join([s+" longtext " for s in sorted_dict_keys]) + ")" 
    
    
    
    query_TableCreate = "CREATE TABLE IF NOT EXISTS "+schema+"."+table_name+" "+ColTypes+"  ROW_FORMAT=COMPACT "
    cursor.execute(query_TableCreate)
    
    #query_TableAlter = "ALTER TABLE "+schema+"."+table_name+"  ENGINE=InnoDB  ROW_FORMAT=COMPRESSED  KEY_BLOCK_SIZE=8;"
    #cursor.execute(query_TableAlter) 
    conn.commit()
    #--------------------------------------------------------------------------
  
    
    # XXX tablename not sanitized
    # XXX test for allowed keys is case-sensitive

    # filter out keys that are not column names
    cursor.execute("describe %s.%s" % (schema , table_name))
    allowed_keys = set(row[0] for row in cursor.fetchall())
    keys         = allowed_keys.intersection(myDict)
    
    # check to see if there is more columns than dict keys
    if len(myDict) < len(keys):
        unknown_keys = allowed_keys - set(myDict) 
        print >> sys.stderr, "skipping keys:", ", ".join(unknown_keys)

    columns = ", ".join(keys)
    values_template = ", ".join(["%s"] * len(keys))

    sql = "insert into %s.%s (%s) values (%s)" % (
        schema,table_name, columns, values_template)
    values = tuple(myDict[key] for key in keys)
    
    
    #---------------------------------------------
    # write in ts format, each cell a time series
    if Format=="NoSQL":
    
        NewVals = []
        for i,h in enumerate(values):
            if np.shape(h)==():
                h = [h]
            atemp = map(str,h)
            NewVals.append(",".join(atemp))
            
        cursor.execute(sql, NewVals)
    #--------------------------------------------
    # write in sql type format
    elif Format=="SQL" : # write in SQL format
        try:
            numrows = np.size(values,1)
        except:
            numrows = 1
        if numrows>1: 
            for i in range(numrows):
                temprow = [a[i] for a in values]
                cursor.execute(sql, temprow)
        else:
            temprow = values
            cursor.execute(sql, temprow)
                    
    conn.commit()
    return conn , cursor




def SortedListSet(seq):
        seen = set()
        seen_add = seen.add
        return [x for x in seq if not (x in seen or seen_add(x))]    

##############################################################################################################################################################
